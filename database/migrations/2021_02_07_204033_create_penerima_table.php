<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenerimaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penerima', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_ktp', 16);
            $table->text('alamat');
            $table->string('nama_lengkap', 30);
            $table->unsignedBigInteger('jenis_bantuan_id');
            $table->foreign('jenis_bantuan_id')->references('id')->on('jenis_bantuan');
            $table->unsignedBigInteger('desa_kel_id');
            $table->foreign('desa_kel_id')->references('id')->on('desa_kel');
            $table->unsignedBigInteger('kecamatan_id');
            $table->foreign('kecamatan_id')->references('id')->on('kecamatan');
            $table->unsignedBigInteger('kab_kota_id');
            $table->foreign('kab_kota_id')->references('id')->on('kab_kota');
            $table->unsignedBigInteger('provinsi_id');
            $table->foreign('provinsi_id')->references('id')->on('provinsi');
            $table->unsignedBigInteger('users_id');
            $table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penerima');
    }
}
