<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';
    protected $guarded = [];

    public function author(){
        return $this->belongsTo('App\User', 'users_id');
    }
}
