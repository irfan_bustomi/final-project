<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penerima extends Model
{
    protected $table = 'penerima';
    protected $guarded = [];

    public function ambildataprovinsi(){
        return $this->belongsTo('App\Provinsi', 'provinsi_id');
    }
    public function ambildatakabupaten(){
        return $this->belongsTo('App\Kabupaten', 'kab_kota_id');
    }
    public function ambildatakecamatan(){
        return $this->belongsTo('App\Kecamatan', 'kecamatan_id');
    }
    public function ambildatadesa(){
        return $this->belongsTo('App\Desa', 'desa_kel_id');
    }
    public function ambildatabantuan(){
        return $this->belongsTo('App\Bantuan', 'jenis_bantuan_id');
    }

}
