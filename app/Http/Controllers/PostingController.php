<?php

namespace App\Http\Controllers;

use App\Posting;
use App\Komentar;
use App\User;
use App\Tag;
use Illuminate\Http\Request;
use DB;
use Auth;

class PostingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
        // $this->middleware('auth')->except(['index']);
        // $this->middleware('auth')->only(['create','edit','update','store','index']);
    }
    public function index()
    {
        // $posting = Posting::all();
        $posting = Posting::with(['komentar'])->orderBy('created_at', 'DESC')->get();
        // dd($posting);
        // $komentar = Komentar::where('posting_id','2')->get();
        return view('items.posting.index', compact('posting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('items.posting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'isi' => 'required'
        ]);
        // dd($request->tags);
        $tags_arr = explode(',', $request["tags"]);
            // dd($tags_arr);

        $tag_ids = [];
        foreach($tags_arr as $tag_name){
            $tag = Tag::where("tag_name", $tag_name)->first();
            if($tag){
                $tag_ids[] = $tag->id;

            }else{
                $new_tag = Tag::create(["tag_name" => $tag_name]);
                $tag_ids[] = $new_tag->id;
            }
        }

        // dd($tag_ids);
        
            

        $posting = Posting::create([
            "isi" => $request["isi"],
            "users_id" => Auth::id()
        ]);

        $posting->tags()->sync($tag_ids);

        

        return redirect('/posting')->with('success', 'Postingan Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Posting  $posting
     * @return \Illuminate\Http\Response
     */
    public function show(Posting $posting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Posting  $posting
     * @return \Illuminate\Http\Response
     */
    public function edit(Posting $posting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Posting  $posting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Posting $posting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Posting  $posting
     * @return \Illuminate\Http\Response
     */
    public function destroy($posting)
    {   
        // dd($posting);
        // $data = Posting::findOrfail($posting);
        // $data->relasikomentar()->delete();
        // Posting::destroy($posting);

        Posting::destroy($posting);
        

        return redirect('/posting')->with('success', 'Postingan berhasil dihapus!');    
    }
}
