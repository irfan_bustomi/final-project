<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Desa;
use Auth;
use DB;

class DesaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
        // $this->middleware('auth')->except(['index']);
        // $this->middleware('auth')->only(['create','edit','update','store','index']);
    }

    public function index()
    {
        $desa = Desa::all(); //klo menampilkan banyak data
        //dd($desa);
        return view('items.desa.index', compact('desa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('items.desa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 
            'nama_desaKel' => 'required'
        ]);
        
        $desa = Desa::create([    
            "nama_desaKel" => $request["nama_desaKel"], 
        ]);
        //dd($desa);
        return redirect('/desa')->with('success', 'Tambah Desa/Kelurahan Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
