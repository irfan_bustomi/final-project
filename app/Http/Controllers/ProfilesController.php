<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use DB;
use Auth;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
        // $this->middleware('auth')->except(['index']);
        // $this->middleware('auth')->only(['create','edit','update','store','index']);
    }
    public function index()
    {
        // $profiles = Profiles::all();
        // dd($profiles->author);
        // $id = Auth::id();
        // $user = Auth::user();
        // $cek = Profile::where('user_id', $id)->get();
        // $cek = Profile::firstOrCreate(['user_id' => $id]);
        // dd($user);
        
        // $ceks = Profile::where('user_id', '2')->get();
        // dd($ceks);
        // $profile_id = Auth::user()->profiles->fullname;
        // $profile_id = Auth::user()->profiles;
        $dataprofil = Auth::user()->dataprofil->id;
        $profiles = Profile::find($dataprofil);
        
        return view('items.profiles.index', compact('profiles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fullname' => 'required',
            'phone' => 'required'
        ]);
            // dd($request);
        $profiles = Profiles::create([
            "fullname" => $request["fullname"],
            "phone" => $request["phone"],
            "photo" => "foto.jpg",
            "users_id" => Auth::id()
        ]);

        // $user = Auth::user();
        // $user->profiles()->save($profiles);

        return redirect('/profiles')->with('success', 'Profil Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function show(Profiles $profiles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function edit(Profiles $profiles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function update($profiles, Request $request){
    
        
        // dd($request->all());    
            
        $update = Profile::where('id', $profiles)->update([
            "full_name" => $request["full_name"],
            "phone" => $request["phone"]
        ]);
        if($request->hasFile('photo')){
            $request->file('photo')->move('img',$request->file('photo')->getClientOriginalName());
            $update = Profile::where('id', $profiles)->update([
                "photo" => $request->file('photo')->getClientOriginalName()
            ]);
        }

        return redirect('/profiles')->with('success', 'Task Created Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profiles  $profiles
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profiles $profiles)
    {
        //
    }
}
