<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Profile;

class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        // $this->middleware('auth')->except(['index']);
        // $this->middleware('auth')->only(['create','edit','update','store','index']);
    }
    public function index()
    {
        $id = Auth::id();
        $user = Auth::user();
        // $cek = Profile::where('user_id', $id)->get();
        $cek = Profile::firstOrCreate(['user_id' => $id]);
        return view('index');
    }
}
