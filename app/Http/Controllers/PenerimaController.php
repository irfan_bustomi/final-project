<?php

namespace App\Http\Controllers;

use App\Penerima;
use App\Provinsi;
use App\Kabupaten;
use App\Kecamatan;
use App\Desa;
use App\Bantuan;
use App\User;
use Illuminate\Http\Request;
use DB;
use Auth;

class PenerimaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
        // $this->middleware('auth')->except(['index']);
        // $this->middleware('auth')->only(['create','edit','update','store','index']);
    }

    public function index()
    {
        $penerima   = Penerima::all();
        $provinsi   = Provinsi::all();
        $kabupaten  = Kabupaten::all();
        $kecamatan  = Kecamatan::all();
        $desa       = Desa::all();
        $bantuan    = Bantuan::all();
        // dd($penerima->ambildatakabupaten);
        //$bantuan    = bantuan::with(['penerima','penerima.jenis_bantuan_id'])->get();
        return view('items.penerima.index', compact('penerima','provinsi','kabupaten','kecamatan','desa','bantuan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $provinsi      = Provinsi::all();
        $kabupaten     = Kabupaten::all();
        $kecamatan     = Kecamatan::all();        
        $desa          = Desa::all();
        $bantuan       = Bantuan::all();
        return view('items.penerima.create', compact('provinsi','kabupaten','kecamatan','desa','bantuan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'no_ktp'       => 'required',
            'nama_lengkap' => 'required'
        ]);
            // dd($request);
       
        $penerima  = Penerima::create([
            "no_ktp"            => $request["no_ktp"],
            "alamat"            => $request["alamat"], 
            "jenis_bantuan_id"  => $request["bantuan"],
            "desa_kel_id"       => $request["desa"],
            "kecamatan_id"      => $request["kecamatan"],
            "kab_kota_id"       => $request["kabupaten"],
            "nama_lengkap"      => $request["nama_lengkap"],
            "provinsi_id"       => $request["provinsi"],
            "users_id" => Auth::id()
        ]);

        return redirect('/penerima')->with('success', 'Penerima Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Penerima  $penerima
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penerima = Penerima::find($id)->first();
        //dd($pertanyaan->author);
        // $penerima   = Penerima::all()->first();
        // $provinsi   = Provinsi::all();
        // $kabupaten  = Kabupaten::all();
        // $kecamatan  = Kecamatan::all();
        // $desa       = Desa::all();
        // $bantuan    = Bantuan::all();
        // dd($penerima->ambildataprovinsi);
        return view('items.penerima.show', compact('penerima'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Penerima  $penerima
     * @return \Illuminate\Http\Response
     */
    public function edit($penerima)
    {   
        $penerima = Penerima::find($penerima);
        $provinsi      = Provinsi::all();
        $kabupaten     = Kabupaten::all();
        $kecamatan     = Kecamatan::all();        
        $desa          = Desa::all();
        $bantuan       = Bantuan::all();
        return view('items.penerima.edit', compact('penerima','provinsi','kabupaten','kecamatan','desa','bantuan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Penerima  $penerima
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $penerima)
    {
        $request->validate([
            'no_ktp'       => 'required',
            'nama_lengkap' => 'required',
            'bantuan' => 'required',
            'desa' => 'required',
            'kecamatan' => 'required',
            'kabupaten' => 'required',
            'alamat' => 'required',
            'provinsi' => 'required'
        ]);
            // dd($request);
       
        $penerima  = Penerima::where('id', $penerima)->update([
            "no_ktp"            => $request["no_ktp"],
            "alamat"            => $request["alamat"], 
            "jenis_bantuan_id"  => $request["bantuan"],
            "desa_kel_id"       => $request["desa"],
            "kecamatan_id"      => $request["kecamatan"],
            "kab_kota_id"       => $request["kabupaten"],
            "nama_lengkap"      => $request["nama_lengkap"],
            "provinsi_id"       => $request["provinsi"]
        ]);


        return redirect('/penerima')->with('success', 'Penerima berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Penerima  $penerima
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Penerima::destroy($id);
        return redirect('/penerima')->with('success', 'Penerima Berhasil dihapus');
    }
}
