<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bantuan extends Model
{
    protected $table = 'jenis_bantuan';
    protected $guarded = [];
}