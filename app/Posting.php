<?php

namespace App;

// use App\Contracts\Likeable;
// use App\Concerns;
use Illuminate\Database\Eloquent\Model;


class Posting extends Model
{
    // use Concerns\Likeable;

    protected $table = 'posting';
    protected $guarded = [];

    public function komentar() {
        return $this->hasMany('App\Komentar', 'posting_id');
    }

    public function author(){
        return $this->belongsTo('App\User', 'users_id');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag', 'posting_has_tags', 'post_id', 'tag_id');
    }
    

}


