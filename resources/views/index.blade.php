@extends('adminLTE.master')

@section('content')
<!-- Daterange picker -->
<link rel="stylesheet" href="{{asset ('/adminLTE/plugins/daterangepicker/daterangepicker.css')}}">

<div class="container-fluid">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">ERD</h3>
      </div>
      <!-- /.card-header -->
      <div style="text-align: center" class="card-body">
       <img height="800px" src="{{'img/erd.png'}}" alt="">
      </div>
      <!-- /.card-body -->
    
    </div>
    <!-- /.card -->

   
  </div>
  </div><!-- /.container-fluid -->
@endsection

@push('stack')
@include('sweetalert::alert')
@endpush
