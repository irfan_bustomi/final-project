@extends('adminLTE.master')

@section('content')

<div class="ml-3 mt-3">
    <!-- general form elements -->
    <div class="card card-primary">
      @if (session('success'))
                  <div class="alert alert-success">
                      {{session('success')}}
                  </div>
              @endif  
      <div class="card-header">
        <h3 class="card-title">Tambah Penerima</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form role="form" action="{{ route('penerima.update', ['penerima' => $penerima->id])}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Nomor KTP</label>
            <input required type="text" value="{{old('no_ktp',$penerima->no_ktp)}}"  class="form-control" name="no_ktp" id="no_ktp" placeholder="Nomor KTP">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Nama Lengkap</label>
            <input required type="text" value="{{old('no_ktp',$penerima->nama_lengkap)}}" class="form-control" name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap">
          </div>
         
          <div class="form-group">
            <label for="exampleInputEmail1">Provinsi</label>
            <select required name="provinsi" id="provinsi" class="form-control select2" style="width: 100%;">
              <option selected="selected">Provinsi</option>
              @foreach ($provinsi as $id => $myProvinsi)
                  <option value="{{$myProvinsi->id}}">{{$myProvinsi->nama_provinsi}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Kabupaten Kota</label>
            <select required name="kabupaten" id="kabupaten" class="form-control select2" style="width: 100%;">
              <option selected="selected">Pilih Kabupaten / Kota</option>
              @foreach ($kabupaten as $id => $myKabupaten)
                  <option value="{{$myKabupaten->id}}">{{$myKabupaten->nama_kabKota}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Kecamatan</label>
            <select required name="kecamatan" id="kecamatan" class="form-control select2" style="width: 100%;">
              <option selected="selected">Pilih Kecamatan</option>
              @foreach ($kecamatan as $id => $mykecamatan)
                  <option value="{{$mykecamatan->id}}">{{$mykecamatan->nama_kecamatan}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Desa / Kelurahan</label>
            <select required name="desa" id="desa" class="form-control select2" style="width: 100%;">
              <option selected="selected">PIlih Desa / Kelurahan</option>
              @foreach ($desa as $id => $myDesa)
                  <option value="{{$myDesa->id}}">{{$myDesa->nama_desaKel}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Alamat</label>
            <textarea required class="form-control" name="alamat" id="alamat" cols="20" rows="5">{{$penerima->alamat}}</textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Jenis Bantuan</label>
            <select required name="bantuan" id="bantuan" class="form-control select2" style="width: 100%;">
              <option selected="selected">Pilih Jenis Bantuan</option>
              @foreach ($bantuan as $id => $myBantuan)
                  <option value="{{$myBantuan->id}}">{{$myBantuan->nama_bantuan}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.card -->

  </div>
  <!--/.col (left) -->
@endsection

@push('stack')
<!-- Select2 -->
<script src="{{ asset ('/adminLTE/plugins/select2/js/select2.full.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset ('/adminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script>
    $(function () {
      //Initialize Select2 Elements
      $('.select2').select2()
  
      //Initialize Select2 Elements
      $('.select2bs4').select2({
        theme: 'bootstrap4'
      })
    })
</script>
@endpush
