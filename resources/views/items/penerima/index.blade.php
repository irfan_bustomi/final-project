@extends('adminLTE.master')

@section('content')
    <div class="mt-2 ml-3">
        <div class="card-header">
            <h3 class="card-title">Data Penerima</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success')}}
                </div>
            @endif
            <a href="/penerima/create" class="btn btn-primary mb-2">Tambah</a>
            <table class="table table-bordered">
              <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nomor KTP</th>
                    <th scope="col">Nama Lengkap</th>
                    <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse($penerima as $key => $value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->no_ktp}}</td>
                    <td>{{$value->nama_lengkap}}</td>
                    <td style="display:flex">
                        <a href="/penerima/{{$value->id}}" class="btn btn-info btn-sm">Show</a>&nbsp;
                        <a href="/penerima/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>&nbsp;
                        <form action="/penerima/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                    <tr colspan="3">
                        <td colspan="8" align="center">No data</td>
                    </tr>  
                @endforelse
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
    </div>
@endsection

@push('script')
@include('sweetalert::alert')

@endpush



