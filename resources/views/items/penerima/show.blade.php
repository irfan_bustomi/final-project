@extends('adminLTE.master')

@section('content')

<div class="ml-3 mt-3">
    <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Penerima</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Nomor KTP</label>
            <input readonly type="text" class="form-control" name="no_ktp" id="no_ktp" value="{{$penerima->no_ktp}}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Nama Lengkap</label>
            <input readonly type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" value="{{$penerima->nama_lengkap}}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Provinsi</label>
            <input readonly type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" value="{{$penerima->ambildataprovinsi->nama_provinsi}}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Kabupaten</label>
            <input readonly type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" value="{{$penerima->ambildatakabupaten->nama_kabKota}}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Kecamatan</label>
            <input readonly type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" value="{{$penerima->ambildatakecamatan->nama_kecamatan}}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Desa</label>
            <input readonly type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" value="{{$penerima->ambildatadesa->nama_desaKel}}">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Alamat</label>
            <textarea readonly class="form-control" name="alamat" id="alamat" cols="20" rows="5">{{$penerima->alamat}}</textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Jenis Bantuan</label>
            <input readonly type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" value="{{$penerima->ambildatabantuan->nama_bantuan}}">
          </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </div>
  <!--/.col (left) -->
@endsection

