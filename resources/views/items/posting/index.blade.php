@extends('adminLTE.master')

@push('script-head')
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
@endpush

@section('content')
<div class="ml-3 mt-3">
  <div class="card card-primary">
    {{-- @if (session('success'))
                  <div class="alert alert-success">
                      {{session('success')}}
                  </div>
              @endif   --}}
    <div class="card-header">
      <h3 class="card-title">Helpdesk</h3>
    </div>
    <!-- /.card-header -->
    

 {{-- @foreach ($posting as $key => $myposting)
 <p>This is user {{ $myposting->id }}</p>

 @foreach ($posting->komentar as $komentar)
 <p>This is user {{ $komentar->id }}</p>
 @endforeach
@endforeach --}}
    <!-- form start -->
    <form role="form" action="{{ route('posting.store')}}" method="POST">
      @csrf
      <div class="card-body">
          <div class="form-group">
              <label for="exampleInputEmail1">Buat Pertanyaan / Diskusi</label>
              {{-- <textarea class="form-control" name="isi" id="isi" cols="20" rows="5"></textarea> --}}
              <textarea name="isi" class="form-control my-editor">{!! old('isi', $isi ?? '') !!}</textarea>
              @error('full_name')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror  
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Kategori Pertanyaan</label>
            <input type="text" name="tags" value="{{old('tags','')}}" class="form-control" id="exampleInputPassword1" placeholder="Masukan tags dipisahkan dengan , (koma)">
            @error('tags')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror  
        </div>
          <button type="submit" class="btn btn-primary">Kirim</button>
       
      </div>
      
    </div>

    </form>
  </div>



    @forelse ($posting as $key => $myposting)
    
    <div class="ml-3 mt-3">
      <!-- Box Comment -->
      <div class="card card-widget">
        <div class="card-header">
          <div class="user-block">
            <img class="img-circle" src="{{ asset ('/img/avatar.png')}}" alt="User Image">
            <span class="username"><a href="#">{{$myposting->author->name}}</a></span>
            <span class="description">{{$myposting->author->email}} - {{$myposting->created_at}}</span>
          </div>
          <!-- /.user-block -->
          <div class="card-tools">
            {{-- <button type="button" class="btn btn-tool" title="Mark as read">
              <i class="far fa-circle"></i>
            </button> --}}
            <?php
            $id = Auth::id();
            $id2 = $myposting->author->id;
           
            ?>
        @if ($id == $id2)
        <form action="{{ route('posting.destroy', ['posting' => $myposting->id])}}" method="POST">
          @csrf
          @method('DELETE')
          <input type="submit" value="delete" class="btn btn-tool swal-confirm">
      </form>
        @else
        
        @endif
            
            {{-- <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button> --}}
            
            {{-- <a class="btn btn-tool swal-confirm" href="">  <i class="fas fa-times"></i></a> --}}
            {{-- <button type="button" class="btn btn-tool" data-card-widget="remove">
              <i class="fas fa-times"></i>
            </button> --}}
          </div>
          <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <!-- post text -->
          Kategori :
          @forelse ($myposting->tags as $tag)
              <button class="btn btn-primary btn-sm">{{$tag->tag_name}}</button>
          @empty
          Tidak ada tags
          @endforelse
          <hr>
          {!!$myposting -> isi!!}

        </div>
        @forelse ($myposting->komentar as $key => $mycomment)
        <div class="card-footer card-comments">
          <div class="card-comment">
            <img class="img-circle img-sm" src="{{ asset ('/img/avatar.png')}}" alt="User Image">
            <div class="comment-text">
              <span class="username">
                {{$mycomment->author->name}}
                <span class="text-muted float-right">{{$mycomment->created_at}}</span>
              </span>
              {{$mycomment->isi}}
            </div>
          </div>
        </div>
        @empty

    <div class="ml-3 mt-3">
      <div class="card card-widget">
        <div class="card-header">
          <span>Tidak Ada Komentar</span>
        </div>
      </div>
    </div>

    @endforelse
        <!-- /.card-footer -->
        <div class="card-footer">
          <?php
          $photo = Auth::user()->dataprofil->photo;
if($photo == NULL){
        $photo = "avatar.png";
      }else{
        $photo = Auth::user()->dataprofil->photo;
      };
          ?>
            <img class="img-fluid img-circle img-sm" src="{{ asset ('/img/'.$photo.'')}}" alt="Alt Text">
            <!-- .img-push is used to add margin to elements next to floating images -->
            <div class="img-push">
              <form role="form2" action="{{ route('komentar.store')}}" method="POST">
                @csrf
                <div class="input-group">
                  <input type="hidden" name="posting_id" value="{{$myposting->id}}" placeholder="Isi Komentar Anda" class="form-control">
                  <input type="text" name="komentar" placeholder="Isi Komentar Anda" class="form-control">
                  <span class="input-group-append">
                    <button type="submit" class="btn btn-primary">Kirim</button>
                  </span>
                </div>
              </form>
            </div>
  
        </div>
        <!-- /.card-footer -->
      </div>
      <!-- /.card -->
    </div>        
    @empty
    <div class="ml-3 mt-3">
      <!-- Box Comment -->
      <div class="card card-widget">
        <div class="card-header">
          <span>Tidak Ada Postingan</span>
        </div>
      </div>
    </div>

  @endforelse

@endsection

@push('script')
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
<script>

</script>
@include('sweetalert::alert')
@endpush