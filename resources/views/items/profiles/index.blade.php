@extends('adminLTE.master')

@section('content')
<div class="ml-3 mt-3">
    <div class="card card-primary">
      {{-- @if (session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif   --}}
      <div class="card-header">
        <h3 class="card-title">Edit Data Profil</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form role="form" action="{{ route('profiles.update', ['profile' => $profiles->id])}}" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Full Name</label>
                <?php 
                  if($profiles->full_name == NULL){
                    $fullname = Auth::user()->name;
                  }else{
                    $fullname = $profiles->full_name;
                  }; 
                ?>
                <input type="text" name="full_name" value="{{old('full_name',$fullname)}}" class="form-control" id="exampleInputPassword1" placeholder="Masukan Nama Lengkap">
                @error('full_name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror  
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Nomor HP</label>
                <input type="text" name="phone" value="{{old('phone',$profiles->phone)}}" class="form-control" id="exampleInputPassword1" placeholder="Masukan Nomor HP">
                @error('phone')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror  
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Foto Profil</label>
              <input type="file" name="photo" class="form-control" placeholder="Upload Foto Profil">
              @error('phone')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror  
          </div>
        </div>
        
      </div>
        <!-- /.card-body -->
  
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Update Data</button>
        </div>
      </form>
    </div>
  </div>
@endsection

@push('script')
@include('sweetalert::alert')
@endpush