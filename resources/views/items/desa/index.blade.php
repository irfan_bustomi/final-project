@extends('adminLTE.master')

@section('content')

    <div class="card">
       @if (session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
        @endif  
        <div class="card-header">
        <h3 class="card-title">DataTable with default features</h3>
        </div>
        <!-- /.card-header -->

        <div class="card-body">
        <button type="button" class="btn btn-secondary mb-2" data-toggle="modal" data-target="#modal-default">
            Tambah
        </button>
        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
            <form role="form" action="{{ route('desa.store')}}" method="POST">
              @csrf
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Desa</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Desa / Kelurahan</label>
                      <input type="text" name="nama_desaKel" value="{{ old('nama_desaKel','')}}" class="form-control" id="nama_desaKel" placeholder="Nama Desa / Kelurahan">
                      @error('nama_desaKel')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </form>
            </div>
            <!-- /.modal-dialog -->
          </div>
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
            <th>#</th>
            <th>Kecamatan</th>
            <th>Nama Desa</th>
            <th>Created At</th>
            <th>Update At</th>
            <th>Action</th>
            </tr>
            </thead>
            <tbody>
              @foreach($desa as $key => $value)
              <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->kecamatan_id}}</td>
                <td>{{$value->nama_desaKel}}</td>
                <td>{{$value->created_at}}</td>
                <td>{{$value->updated_at}}</td>
                <td style="display:flex">
                  <a href="{{ route('desa.show', ['desa' => $value->id ]) }}" class="btn btn-info btn-sm">Show</a>
                  <a href="{{route('desa.edit', ['desa' => $value->id])}}">Edit</a>
                  <form action="{{ route('desa.destroy', ['desa' => $value->id])}}" method="POST">
                      @csrf
                      @method('DELETE')
                      <input type="submit" value="delete" class="btn btn-danger btn-sm">
                  </form>
                </td>
              </tr>

              @endforeach
            </tfoot>
        </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@endsection

@push('script')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset ('/adminLTE/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset ('/adminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset ('/adminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset ('/adminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{ asset ('/adminLTE/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset ('/adminLTE/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ asset ('/adminLTE/plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{ asset ('/adminLTE/plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{ asset ('/adminLTE/plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{ asset ('/adminLTE/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset ('/adminLTE/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset ('/adminLTE/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        });
    });
    </script>
@endpush