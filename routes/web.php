<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/items', function(){
    return view('items.desa.index');
});



Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard', 'DashboardController@index');
Route::get('/', 'DashboardController@index');

// Route::get('/', function () {
//     return view('index');
// });


Route::resource('desa', 'DesaController');
Route::resource('posting', 'PostingController');
Route::resource('profiles', 'ProfilesController');
Route::resource('penerima', 'PenerimaController');
Route::resource('komentar', 'KomentarController');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
    
});

Route::post('like', 'LikesController@like');
Route::delete('like', 'LikesController@dislike');